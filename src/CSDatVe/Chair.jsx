import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction } from "../store/BTDatve/action";
import cn from "classnames";
import "../Css/custom.css";
const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  return (
    <button 
      className={cn("btn btn-outline-dark Chair", {
        booking: chairBookings.find((e) => e.soGhe === ghe.soGhe),
        booked: chairBookeds.find((e) => e.soGhe === ghe.soGhe),
      })}
      style={{ width: "60px" }}
      onClick={() => {
        dispatch(chairBookingsAction(ghe));
      }}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
