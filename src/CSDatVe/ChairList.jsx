import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  return (
    <div>
      {data.map((hangGhe) => {
        return (
          <div className="d-flex mt-3" style={{ gap: "10px" }}>
            <p className="text-center" style={{ width: "40px" }}>
              {hangGhe.hang}
            </p>
            <div className="d-flex" style={{ gap: "10px 15px" }}>
              {hangGhe.danhSachGhe.map((ghe) => {
                return <Chair ghe={ghe} key={ghe.soGhe} />;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
