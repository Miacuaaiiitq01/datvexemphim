import React from "react";
import ChairList from "./ChairList";
import Result from "./Result";
import data from "./data.json";

const DatVe = () => {
  return (
    <div
      className="container-fluid text-center mt-2"
      style={
        {
          // position: "fixed",
          // width: "100%",
          // height: "100%",
          // backgroundSize: "cover",
        }
      }
    >
      <h1>BTDatVe</h1>
      <div className="row">
        <div className="col-8">
          <h1 className="display-4">Đặt vé xem phim</h1>
          <div className="text-center p-1 font-weight-bold display-4 bg-dark text-white mt-3">
            SCREEN
          </div>
          {/* Danh sách ghế */}
          <ChairList data={data} />
        </div>
        <div className="col-4">
          {/* Kết quả đặt vé */}
          <Result />
        </div>
      </div>
    </div>
  );
};

export default DatVe;
