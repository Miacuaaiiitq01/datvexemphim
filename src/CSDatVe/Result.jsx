import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction, payAction } from "../store/BTDatve/action";

const Result = () => {
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>Dánh sách ghế bạn đã chọn</h1>
      <div className="d-flex">
        <button className="btn btn-dark text-light"> Ghế đã đặt</button>
        <button
          className="btn btn-outline-light text-dark Chair ml-3"
          style={{ border: "1px solid black" }}
        >
          Ghế chưa đặt
        </button>
        <button className="btn btn-outline-danger Chair ml-3">
          Ghế đang chọn
        </button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(chairBookingsAction(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}
          <tr>
            <td>Tổng tiền</td>
            <td>
              {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>
      <button
        className=" btn btn-success"
        onClick={() => {
          dispatch(payAction());
        }}
      >
        Thanh toán
      </button>
    </div>
  );
};

export default Result;
