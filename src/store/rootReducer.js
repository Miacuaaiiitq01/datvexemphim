import { combineReducers } from "redux";
import demoReduxReducer from "./Redux/reducer";
import { btDatveReducer } from "./BTDatve/reducer";

export const rootReducer = combineReducers({
  Redux: demoReduxReducer,
  btDatVe: btDatveReducer,
});
